#include <iostream>
#include <thread>
#include <Windows.h>
#include "Element.h"
#include "Problem.h"
#include "Solution.h"

#define	SEP ";"


using namespace std;

int main(int argc, char **argv)
{
	bool loading = false;
	bool ameliorate = false;
	char generate = 'N';
	Problem *all_instancies;
	ofstream output_file;
	Solution *solution;
	Solution *solution_ameliorate;
	cout << "Reading files... " << endl;
	all_instancies = Problem::loadInstancies(loading);
	cout << "Resolving and getting constructive solution... " << endl;
	solution = Problem::resolveAll(all_instancies);
	cout << "Ameliorating solutions.." << endl;
	solution_ameliorate = Problem::ameliorateAll(all_instancies, solution);
	cout << "Do you want to generate a CSV file? (O/N) ";
	cin >> generate;
	switch (generate)
	{
	case 'O':
	case 'o':
		output_file.open("result.csv");
		output_file << "Problem;Max-Weight;Solution-Weight;Amelioration-Weight;Solution-value;Amelioration-value;" << endl;
		for (int i = 0; i < INSTANCE; i++)
		{
			output_file << i + 1 << SEP << all_instancies[i].getMaxSize() <<  SEP << Problem::sizingSolution(solution[i], all_instancies[i]) << SEP << Problem::sizingSolution(solution_ameliorate[i], all_instancies[i]) << SEP << Problem::valuingSolution(solution[i], all_instancies[i]) << SEP << Problem::valuingSolution(solution_ameliorate[i], all_instancies[i]) <<  SEP << endl;
		}
		output_file.close();
		break;
	case 'N':
	case 'n':
	default:
		break;
	}
	for (int i = 0; i < INSTANCE; i++)
	{
		if (Problem::valuingSolution(solution[i], all_instancies[i]) < Problem::valuingSolution(solution_ameliorate[i], all_instancies[i]))
		{
			cout << "problem " << i << " ameliorated from " << Problem::valuingSolution(solution[i], all_instancies[i]) <<" to " << Problem::valuingSolution(solution_ameliorate[i], all_instancies[i]) << endl;
			cout << "------------------" <<endl;
			ameliorate = true;
		}
	}
	if (!ameliorate)
		cout << "No amelioration found." << endl;
	return 0;
}