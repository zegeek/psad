#include "Element.h"

using namespace std;

Element::Element()
{
}

Element::Element(short int weight, short int value)
{
	this->weight = weight;
	this->value = value;
}

short int Element::getWeight()
{
	return this->weight;
}

short int Element::getValue()
{
	return this->value;
}

void Element::display(ostream & flux) const
{
	flux << "Weight: " << this->weight << " - Value: " << this->value;
}

Element::~Element()
{
}
ostream& operator<<(ostream& flux, Element const& obj)
{
	obj.display(flux);
	return flux;
}

