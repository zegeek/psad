#include "Problem.h"

using namespace std;

Problem::Problem()
{
}

Problem::Problem(std::string file)
{
	ifstream problem_file;
	problem_file.open(file);
	bool getout = false;
	int step = 0;
	size_t pos;
	string separator = "	";
	short int i = 0;
	string buffer;
	if (problem_file.is_open())
	{
		while (getline(problem_file, buffer) && !getout)
		{
			switch (step)
			{
			case 0:
				this->max_size = stoi(buffer);
				step++;
				break;
			case 1:
				this->elements_amount = stoi(buffer);
				step++;
				this->elements = new Element[this->elements_amount];
				break;
			case 2:
				pos = buffer.find(separator);
				elements[i] = Element((short int)stoi(buffer.substr(0, pos)), (short int)stoi(buffer.substr(pos + 1)));
				i++;
				if (i >= this->elements_amount)
					getout = true;
				break;
			default:
				break;
			}
		}
	}
	problem_file.close();
}

short int Problem::getMaxSize()
{
	return this->max_size;
}
short int Problem::getElementsAmount()
{
	return this->elements_amount;
}
Element* Problem::getElements()
{
	return this->elements;
}

void Problem::display(ostream& flux)
{
	flux << "Max Size of bag: " << this->max_size << " - Amount of available elements: " << this->elements_amount << endl << "Available elements: " << endl;
	for (int i = 0; i < this->elements_amount; i++)
	{
		flux <<"Element "<< i+1 << " " << elements[i] << endl;
	}
}
Element Problem::getElementAt(short int position)
{
	if (position < this->elements_amount)
		return this->elements[position];
	else
		return Element(0, 0);
}
Problem* Problem::loadInstancies(bool &status)
{
	string file_name;
	string position;
	Problem *all_instancies = new Problem[INSTANCE];
	for (int i = 0; i < INSTANCE; i++)
	{
		if (i < 10)
		{
			position = "00" + to_string(i+1);
			file_name = PREFIX + position + EXTENSION;
		}
		else if (i >= 10 && i < 100)
		{
			position = "0" + to_string(i+1);
			file_name = PREFIX + position + EXTENSION;
		}
		else {
			file_name = PREFIX + to_string(i+1) + EXTENSION;
		}
		all_instancies[i] = Problem(file_name);
	}
	status = true;
	return all_instancies;
}
short int Problem::sizingSolution(Solution solution, Problem problem)
{
	short int size = 0;
	for (int i = 0; i < problem.getElementsAmount(); i++)
	{
		size += solution.picked_solution[i] * problem.getElementAt(i).getWeight();
	}
	return size;
}
int Problem::valuingSolution(Solution solution, Problem problem)
{
	int value = 0;
	for (short int i = 0; i < problem.getElementsAmount(); i++)
	{
		value += solution.picked_solution[i] * problem.getElementAt(i).getValue();
	}
	return value;
}
Solution Problem::getConstructiveSolution(Problem problem)
{
	Solution solution;
	solution.picked_solution = (unsigned short int *)malloc(problem.getElementsAmount() * sizeof(unsigned short int));
	bool changed = true;
	double buffer = 0;
	int buffer_2 = 0;
	short int size = 0;
	double *approached = (double *)malloc(problem.getElementsAmount() * sizeof(double));
	int *label = (int *)malloc(problem.getElementsAmount() * sizeof(int));
	for (int i = 0; i < problem.getElementsAmount(); i++)
	{
		solution.picked_solution[i] = 0;
		approached[i] = (double)((double)(problem.getElementAt(i).getValue()) / (double)(problem.getElementAt(i).getWeight()));
		label[i] = i;
	}
	size = Problem::sizingSolution(solution, problem);
	while (changed)
	{
		changed = false;
		for (int i = 1; i < problem.getElementsAmount(); i++)
		{
			if (approached[i] > approached[i - 1])
			{
				buffer = approached[i];
				approached[i] = approached[i - 1];
				approached[i - 1] = buffer;
				buffer_2 = label[i];
				label[i] = label[i - 1];
				label[i - 1] = buffer_2;
				changed = true;
			}
		}
	}
	buffer_2 = 0;
	changed = true;
	do
	{
		solution.picked_solution[label[buffer_2]] = 1;
		buffer_2++;
		size = Problem::sizingSolution(solution, problem);
		if (size > problem.getMaxSize())
		{
			solution.picked_solution[label[buffer_2 - 1]] = 0;
			changed = false;
		}
	}while (changed && buffer_2 < problem.getElementsAmount());
	return solution;
}
Solution Problem::getLocalOptimumSolution(Solution constructive, Problem problem)
{
	int init_value = Problem::valuingSolution(constructive, problem);
	bool change = true;
	unsigned short int permut_buffer;
	//displaySolution(constructive, problem);
	Solution min_solution = Problem::copySolution(constructive,problem);
	Solution min_buffer_solution = Problem::copySolution(constructive, problem);
	while (change)
	{
		change = false;
		for (int i = 0; i < problem.getElementsAmount() - 1; i++)
		{
			if (i + SEARCH_FLAG < problem.getElementsAmount())
			{
				permut_buffer = min_solution.picked_solution[i];
				min_solution.picked_solution[i] = min_solution.picked_solution[i + SEARCH_FLAG];
				min_solution.picked_solution[i + SEARCH_FLAG] = permut_buffer;
				//displaySolution(min_solution, problem);
				if (Problem::valuingSolution(min_solution, problem) > init_value && Problem::sizingSolution(min_solution, problem) <= problem.getMaxSize())
				{
					init_value = Problem::valuingSolution(min_solution, problem);
					min_buffer_solution = Problem::copySolution(min_solution, problem);
					change = true;
				}
			}
			if(change)
				min_solution = Problem::copySolution(min_buffer_solution, problem);
		}
	}
	return min_buffer_solution;
}
void Problem::displaySolution(Solution solution, Problem problem)
{
	for (int i = 0; i < problem.getElementsAmount(); i++)
	{
		cout << solution.picked_solution[i] << " ";
	}
	cout << endl;
}
Solution* Problem::resolveAll(Problem *all_instancies)
{
	Solution *solutions = (Solution *) malloc(INSTANCE * sizeof(Solution));
	for (int i = 0; i < INSTANCE; i++)
	{
		solutions[i] = Problem::getConstructiveSolution(all_instancies[i]);
	}
	return solutions;
}
Solution* Problem::ameliorateAll(Problem *all_instancies, Solution* constructive)
{
	Solution *solutions = (Solution *)malloc(INSTANCE * sizeof(Solution));
	for (int i = 0; i < INSTANCE; i++)
	{
		solutions[i] = Problem::getLocalOptimumSolution(constructive[i], all_instancies[i]);
	}
	return solutions;
}
Solution Problem::copySolution(Solution solution, Problem instance)
{
	Solution copy;
	copy.picked_solution = new unsigned short int[instance.getElementsAmount()];
	for (int i = 0; i < instance.getElementsAmount(); i++)
	{
		copy.picked_solution[i] = solution.picked_solution[i];
	}
	return copy;
}

Problem::~Problem()
{
}
ostream& operator<<(ostream& const flux, Problem &obj)
{
	obj.display(flux);
	return flux;
}