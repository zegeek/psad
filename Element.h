#pragma once
#include <iostream>

class Element
{
	short int weight;
	short int value;
public:
	Element();
	Element(short int weight, short int value);
	short int getWeight();
	short int getValue();
	void display(std::ostream &flux)  const;
	~Element();
};
std::ostream& operator<<(std::ostream& flux, Element const& obj);