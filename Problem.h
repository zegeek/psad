#pragma once
#include <string>
#include <fstream>
#include "Element.h"
#include "Solution.h"

#define PREFIX "psad"
#define EXTENSION ".txt"
#define INSTANCE 200
#define SEARCH_FLAG 2

class Problem
{
	short int max_size;
	short int elements_amount;
	Element *elements;
public:
	Problem();
	Problem(std::string file);
	short int getMaxSize();
	short int getElementsAmount();
	Element* getElements();
	Element getElementAt(short int position);
	static Problem* loadInstancies(bool &status);
	static Solution getConstructiveSolution(Problem problem);
	static Solution getLocalOptimumSolution(Solution constructive, Problem problem);
	static short int sizingSolution(Solution solution, Problem problem);
	static int valuingSolution(Solution solution, Problem problem);
	static Solution* resolveAll(Problem *all_instancies);
	static Solution* ameliorateAll(Problem *all_instancies, Solution* constructive);
	static void displaySolution(Solution solution,Problem problem);
	static Solution copySolution(Solution solution, Problem instance);
	void display(std::ostream& flux);
	~Problem();
};

std::ostream& operator<<(std::ostream& const flux, Problem &obj);